/****************************************************************************
**
** Copyright (C) 2019 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Design Tooling
**
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 as published by the Free Software
** Foundation with exceptions as appearing in the file LICENSE.GPL3-EXCEPT
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
****************************************************************************/
#include "mainwindow.h"
#include "curveeditor.h"
#include "examplecurvemodel.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QVBoxLayout>

#include <functional>

namespace DesignTools {

QHBoxLayout *createSlider(const QString &name, std::function<void(int)> fun)
{
    QSlider *slider = new QSlider;
    slider->setMinimum(0);
    slider->setMaximum(1000);
    slider->setValue(0);
    slider->setOrientation(Qt::Horizontal);
    QObject::connect(slider, &QSlider::valueChanged, fun);

    auto *hbox = new QHBoxLayout;
    hbox->addWidget(new QLabel(name));
    hbox->addWidget(slider);

    return hbox;
}

MainWindow::MainWindow()
    : QMainWindow()
{
    auto *model = new ExampleCurveModel;
    auto *editor = new CurveEditor(model);

    auto zoomX = [editor](int val) { editor->zoomX(static_cast<double>(val) / 1000.); };
    auto zoomY = [editor](int val) { editor->zoomY(static_cast<double>(val) / 1000.); };

    auto *box = new QVBoxLayout;
    box->addWidget(editor);
    box->addLayout(createSlider("Zoom X", zoomX));
    box->addLayout(createSlider("Zoom Y", zoomY));

    auto *centralWidget = new QWidget;
    centralWidget->setLayout(box);
    setCentralWidget(centralWidget);

    resize(QSize(1070, 739));

    model->createItems();

    model->setCurrentFrame(75);
}

} // End namespace DesignTools.
